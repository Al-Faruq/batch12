  
const whatTimeIsIt = time => {
}
  
  
// do not change this code below
const test = (testCase, result) => console.log(testCase === result);

test(whatTimeIsIt("12:00 pm"), "It's noon")
test(whatTimeIsIt("3:49 pm"), "It's three past forty nine in the afternoon")
test(whatTimeIsIt('12:00 am'), "It's midnight")
test(whatTimeIsIt("5:31 am"), "It's five past thirty one in the morning")
test(whatTimeIsIt("9:11 am"), "It's nine past eleven in the morning")

// Description
// Help us to translate the given time into a sentence that states that time it self

// Hint
// time : String time that must be translated into a sentence
// if time = 12:00 am return must be "It's midnight"
// if time = 12:00 pm return must be "It's noon"
// else return must be what time is it and it's afternoon or morning

// Examples
// time = 2:15 pm
// return = It's two past fifteen in the afternoon
// time = 8:57
// return = It's eight past fifty seven in the morning