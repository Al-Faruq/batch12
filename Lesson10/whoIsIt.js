const whoIsit = (firstDay, secondDay) => {
    // do code here
}
  
// do not change this code below
const test = (testCase, result) => {
if (testCase.sort().join() === result.sort().join()) {
    return console.log(true)
}
return console.log(false)
}

test(whoIsit(["Joko", "Ani", "Budi"], ["Joko"]), ['Joko'])
test(whoIsit(["Andi", "Prabowo", "Jokowi", "Roberto"], ["Sebastian", "Rachel", "Jokowi", "Prabowo"]), ["Jokowi", "Prabowo"])
test(whoIsit(["Zoe", 'Pearson', 'Dona', 'Luis'], ["Dona", "Robert", "Forstman", "Katrina"]), ['Dona'])
test(whoIsit(["Badu", 'Gilang', 'Silvy'], ['Amal', 'Adrian', 'Margi']), [])
test(whoIsit(['Ian', 'Aron', 'Udin', 'Adit', 'Natalia', 'Lia'], ['Ian', 'Aron', 'Udin', 'Adit', 'Natalia', 'Lia']), ['Ian', 'Aron', 'Udin', 'Adit', 'Natalia', 'Lia'])

// Description
// We are an event organizer who will hold concert event in 2 days. Visitors can buy ticket for the first day, second day or both. We have provided a list of vistors for the first day and the second day, but we want to know who bought the ticket for two days. Please help us to find out who these people are, the order of the list it doesn't matter

// Hint
// firstDay : List of visitors for the firstDay
// seondDay : List of visitors for the secondDay

// Examples
// Input
// firstDay = ["Anton","Joko",'Mike","Zoe","Badu"]
// seconDay = ["Ani","Budi","Anton","Wowo","Zoe","Bobo"]

// Output
// ["Anton","Zoe"]