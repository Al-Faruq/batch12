const whenWasItPosted = date => {
}
  
  
// do not change this code below
const changeToString = __ => {
  const _ = new Date(__)
  const month = _.toLocaleString('default', { month: 'long' });
  const date = _.getDate()
  const year = _.getFullYear()
  return `${date} ${month} ${year}`
}
const test = (testCase, result) => console.log(testCase === result);

const date = new Date();
const date1 = new Date();
const date2 = new Date();
const date3 = new Date();
const date4 = new Date();

test(whenWasItPosted(changeToString(date.setMonth(date.getMonth() - 11))), '11 months ago')
test(whenWasItPosted(changeToString(date1.setDate(date.getDate() - 14))), '2 weeks ago')
test(whenWasItPosted(changeToString(date2.setDate(date.getDate() - 3))), '3 days ago')
test(whenWasItPosted(changeToString(date3.setFullYear(date.getFullYear() - 1))), '1 year ago')
test(whenWasItPosted(changeToString(date4.setFullYear(date.getFullYear() - 20))), '20 years ago')

// Description
// We have a platform to find job vacancies, we can present job vacancies 
// according to the wishes of job seekers. 
// Currently we want to make improvement to our platform, 
// where every job vacancy that is presented to users 
// also provides information on how long the job has been posted 
// from the current date. Please help us to build this feature

// Hint
// date : Is string of the date it was posted
// when it's less than a week return how many day ago job was posted
// when it's more than equal a week and less than a month return how many week ago job was posted
// when it's more than equal a month and less than a year return how many month ago jow was posted
// when it's more than equal a year return how many year ago job was posted

// Examples
// current date : 13 December 2020
// date posted : 8 December 2020
// return : 5 days ago
// current date : 13 December 2020
// date posted : 19 November 2020
// return : 3 weeks ago
// current date : 13 December 2020
// date posted : 13 September 2020
// return : 3 months ago
// current date : 13 December 2020
// date posted : 13 December 2019
// return : 1 year ago