var str = "Saya Makan Saya"
console.log(`1.${str.slice(1, 5)}.`)
console.log(`2.${str.substring(1, 5)}.`)
console.log(`3.${str.substr(1, 5)}.`)

console.log(`4.${str.slice(1, -1)}.`) // Bisa menggunakan index negatif
console.log(`5.${str.substring(1, -1)}.`) // Tidak bisa menggunakan index negatif
console.log(`6.${str.substr(1, -1)}.`) // Length tidak bisa negatif & 0

console.log(`7.${str.slice(2)}.`) // Otomatis end str.length
console.log(`8.${str.substring(2)}.`) // Otomatis end str.length
console.log(`9.${str.substr(2)}.`) // Otomatis lengthnya str.length

console.log(`10.${str.slice(-2)}.`) // Bisa menggunakan index negatif
console.log(`11.${str.substring(-2)}.`) // Tidak bisa menggunakan index negatif otomatis jadi 0
console.log(`12.${str.substr(-2)}.`) // Bisa menggunakan index negatif