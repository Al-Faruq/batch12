var str = "Saya Makan Saya"

console.log(str.length)
console.log(str.indexOf("Saya"))
console.log(str.search("Saya"))
console.log(str.lastIndexOf("Saya"))

console.log(str.indexOf("Mau"))
console.log(str.search("Mau"))
console.log(str.lastIndexOf("Mau"))

console.log(str.indexOf("Saya", 2))
console.log(str.lastIndexOf("Saya", 10))