var str = "Saya Makan Saya"
console.log(1, str)

str = str.replace("Saya", "Ayam")
console.log(2, str)

var str = "Saya Makan Saya"
str = str.replace(/SaYa/i, "Ayam")
console.log(3, str)

var str = "Saya Makan Saya"
str = str.replace(/Saya/g, "Ayam")
console.log(4, str)

var str = "Saya Makan Saya"
str = str.replace(/SaYa/ig, "Ayam")
console.log(5, str)

// APPLE return APPLE
// DYI return IDYAY