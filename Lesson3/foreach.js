//Clue : hero yang terbaik mengandung huruf "j, a, g, o " didalam weapon
// Tebak siapa hero terbaik nya 
const heroes = [
    { name: "Zilong", weapon: "qwertyujimp"},
    { name: "Uranus", weapon: "zzssdtwmncvaklq"},
    { name: "Faany", weapon: "qwerjzaklgiuop"}
]

function find_best_hero(hero){
    // Untuk Hitung Jumlah Huruf J,A,G,O
    let word = { j: 0, a: 0, g:0, o:0}
    //Looping Pertama Untuk Looping Nama Weapon Per Karakter
    for(let i = 0; i < hero.weapon.length; i++){
        //Looping Kedua Untuk Mengecek Apakah Ada Huruf J,A,G,O didalam weapon
        for (key in word) {
            //Jika Ada Huruf J,A,G,O maka word bertambah 1
            if(hero.weapon[i] == key) word[key]++
        }
    }
    //Menambahkan word kedalam Hero sebagai Result
    hero.result = word
    console.log(hero)
    
    //Looping & Proses Untuk Menghitung Total Jumlah J,A,G,O Dalam Result
    let counter = 0
    for (key in word){
        //Jika J,A,G,O berisikan angka 1 Maka Counter Bertambah
        if(word[key] == 1) counter++
    }
    //Jika counter = 4 Maka Tampilkan Nama Hero
    if(counter == 4) console.log(`${hero.name} is the best hero`)
}

heroes.forEach(hero => console.log(hero))
heroes.forEach(find_best_hero)