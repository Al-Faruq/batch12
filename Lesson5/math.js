console.log(Math.PI)
console.log(Math.random())
console.log(Math.min(1, 2, 3, 4))
console.log(Math.max(1, 2, 3, 4))
console.log(Math.abs(1-15))
console.log(Math.pow(4, 4)) //4, 16, 64, 256
console.log(4**4)
console.log(Math.sqrt(64))

var x = 2.5
console.log("x =", x)
console.log("round", Math.round(x))
console.log("ceil", Math.ceil(x))
console.log("floor", Math.floor(x))
console.log("trunc", Math.trunc(x))
console.log()
x = 2.4
console.log("x =", x)
console.log("round", Math.round(x))
console.log("ceil", Math.ceil(x))
console.log("floor", Math.floor(x))
console.log("trunc", Math.trunc(x))
console.log()
x = -2.5
console.log("x =", x)
console.log("round", Math.round(x))
console.log("ceil", Math.ceil(x))
console.log("floor", Math.floor(x))
console.log("trunc", Math.trunc(x))
console.log()
x = -2.4
console.log("x =", x)
console.log("round", Math.round(x))
console.log("ceil", Math.ceil(x))
console.log("floor", Math.floor(x))
console.log("trunc", Math.trunc(x))
x = -2.6
console.log("x =", x)
console.log("round", Math.round(x))
console.log("ceil", Math.ceil(x))
console.log("floor", Math.floor(x))
console.log("trunc", Math.trunc(x))


x = 7.9862235 // 7.9862235 -> 798.62235 -> 799 -> 7.99
console.log("round", Math.round(x * 100) / 100)

Math.up()