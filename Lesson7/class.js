class Date {
    constructor(years=1970, months=0, days=1, hours=0, minutes=0, seconds=0, miliseconds=0) {
      this.years = years;
      this.months = months;
      this.days = days;
      this.hours = hours;
      this.minutes = minutes;
      this.seconds = seconds;
      this.miliseconds = miliseconds;
    }

    getFullYear(){
        return this.years
    }
    
    setFullYear(years){
        this.years = years
    }
}