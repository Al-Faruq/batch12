var fruits = ["Jackfruit", "Banana", "Orange", "Kiwi", "Apple", "Grape", "Pear"]
console.log(fruits)
// split & slice = splice

// splice(start: number, deleteCount?: number)
// let result = fruits.splice(3)
// console.log(result)
// console.log(fruits)

// let result = fruits.splice(3, 2)
// console.log(result)
// console.log(fruits)

//Not working If delete number bigger than array
// let result = fruits.splice(3, -1)
// console.log(result)
// console.log(fruits)

// let result = fruits.splice(3, 2, "Mango", "Strawberry", "Tomato")
// console.log(result)
// console.log(fruits)

//Tips Adding Element Between Array
let result = fruits.splice(3, 0, "Mango", "Strawberry", "Tomato")
console.log(result)
console.log(fruits)