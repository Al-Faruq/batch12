var fruits = ["Jackfruit", "Banana", "Orange", "Kiwi", "Apple", "Grape", "Pear"]

// console.log(fruits.map(x =>  x += " Wow"))
// console.log(fruits)
// Item in Array will change
// fruits = fruits.map(x =>  x += " Wow")
// console.log(fruits)
// fruits.map((x) => {console.log(x += " Wow")})
// console.log(fruits)
fruits = fruits.map(function(x){
    if(x.length >= 5){
        return x += " Wow"
    }
    return x
})
console.log(fruits)

// Item in Array will not change
// fruits = fruits.forEach(x => x += " Wow")
// console.log(fruits)
// let result = 0
// fruits.forEach((x) => result += x.length)
// console.log(result)
// console.log(fruits)
// fruits = fruits.forEach(function(x){return x += " Wow"})
// console.log(fruits)